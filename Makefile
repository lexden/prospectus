SHELL = /bin/bash

names =	"postcard" "location" "an-introduction" \
	"around-and-about" "fees" "eburru" "montessori" \
	"miscellaneous" "what-we-provide" "activities" \
	"2010-11" "2011-12" "2012-13"

obversepdf:
	for name in $(names); do \
		inkscape --without-gui \
			--export-pdf="1-sides/obverse/3-pdf/$$name.pdf" \
			"1-sides/obverse/2-svg/$$name.svg"; \
		done

joinedpdf: obversepdf
	for name in $(names); do \
		./pdfjoin \
			"1-sides/obverse/3-pdf/$$name.pdf" \
			"1-sides/reverse/3-pdf/$$name.pdf" \
			--outfile "2-pdf/$$name.pdf"; \
		done

trimmedpdf: obversepdf
	for side in "obverse" "reverse"; do \
		for name in $(names); do \
			./pdfnup \
				--nup "1x1" \
				--paper "143.5mm,100mm" \
				--trim "0.5cm 0.5cm 0.5cm 0.5cm" \
				--outfile "1-sides/$$side/4-trimmedpdf/$$name.pdf" \
				"1-sides/$$side/3-pdf/$$name.pdf"; \
			done; \
		done

losslesspng: trimmedpdf
	for side in "obverse" "reverse"; do \
		for name in $(names); do \
			convert \
				-resize 587 \
				-units PixelsPerInch -density 300 \
				"1-sides/$$side/4-trimmedpdf/$$name.pdf" \
				"1-sides/$$side/5-losslesspng/$$name.png"; \
			done; \
		done

webpng: losslesspng
	for side in "obverse" "reverse"; do \
		for name in $(names); do \
			pngnq "1-sides/$$side/5-losslesspng/$$name.png"; \
			mv "1-sides/$$side/5-losslesspng/$$name-nq8.png" "1-sides/$$side/6-webpng/$$name.png"; \
			done; \
		done

clean:
	rm -f 1-sides/obverse/3-pdf/*.pdf
	for side in "obverse" "reverse"; do \
		rm -f 1-sides/$$side/4-trimmedpdf/*.pdf; \
		rm -f 1-sides/$$side/5-losslesspng/*.png; \
		rm -f 1-sides/$$side/6-webpng/*.png; \
	done
	rm -f 2-pdf/*.pdf

all: clean pdf webpng
